var people = document.getElementById("ppl");
var cost = document.getElementById("totalcost");
var slider = document.getElementById("sliderrange");
var budget = document.getElementById("budget");
var rod = document.getElementById("left");
var remaining = document.getElementById("remaining");
var remainingd = document.getElementById("remainingd");
people.innerHTML = slider.value;
cost.innerHTML = Number(slider.value * 10 + 5);
function debt() {
    remaining.innerHTML = budget.value - cost.innerHTML;
    if (Number(cost.innerHTML) >= Number(budget.value)) {
        rod.innerHTML = "debt";
        rod.style.color = "red";
        remaining.style.color = "red";
        remainingd.style.color = "red";
    } else {
        rod.innerHTML = "left to spend";
        rod.style.color = "white";
        remaining.style.color = "white";
        remainingd.style.color = "white";
    }
}
debt();
budget.oninput = function() {debt();}
slider.oninput = function() {
    people.innerHTML = slider.value;
    cost.innerHTML = slider.value * 10 + 5;
    debt();
}
document.getElementById("limitsave").onclick = function() {
    slider.max = document.getElementById("limit").value;
    document.getElementById("slidermax").innerHTML = document.getElementById("limit").value;
}
document.getElementById("corpsesave").onclick = function() {people.innerHTML = Number(Number(slider.value) + Number(document.getElementById("corpses").value));}
document.getElementById("disco").onclick = function() {document.getElementById("centerpiece").style.animation = "multicolor 4s infinite";}
document.getElementById("neg").onclick = function() {
    slider.min = "-32";
    document.getElementById("slidermin").innerHTML = "-32";
}
document.getElementById("sus").onclick = function() {
    new Audio("amogussfx.wav").play();
    new Audio("amogus.wav").play();
}